import React from 'react'
import './App.css'
import Message from './HelloWorld'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Message />
      </header>
    </div>
  );
}

export default App;
